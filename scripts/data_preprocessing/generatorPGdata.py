import sys
import os
import pandas as pd
import random

# Movies dataset, inspired from Neo4j toy example
# Movies, Sequel
# Actor, Person
# MovieDirector, Person
# Characters
# Cinema
# Award

# two parameters: the folder where to store the CSV files and the number of movies

CHARS = "abcdefghijklmnopqrstuvwxyz"
CHARS_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
DIGITS = "0123456789"
NATIONALITIES = ["AF","AL","DZ","AS","D","AO","AI","AG","AR","CV","KH","KY","CL","TD","FR","IT","DZ","ES","EN","CZ","GK"]
COUNTER_ID = 0
MAX_CHARS = len(CHARS)-1
MAX_DIGITS = len(DIGITS)-1
MAX_NATIONALITIES = len(NATIONALITIES)-1


def random_string(nb_chars):
    string = ""
    string = CHARS_CAPS[random.randint(0, MAX_CHARS)]
    for _ in range(nb_chars):
        string = f'{string}{CHARS[random.randint(0, MAX_CHARS)]}'
    return string


def random_year():
    return random.randint(1960, 2030)


def random_int():
    return random.randint(90, 300)


def random_large_int():
    return random.randint(100000, 5000000)


def random_text(nb_words):
    text = ""
    for _ in range(nb_words):
        text = f'{text} {random_string(random.randint(1,random.randint(1, 10)))}'
    return text


def random_date():
    return str(random.randint(1, 31)) + "/" + str(random.randint(1, 12)) + "/" + str(random_year())


def random_nationality():
    return NATIONALITIES[random.randint(0, MAX_NATIONALITIES)]


# return id, counter
def create_id(node_label, counter):
    return (node_label + str(counter)), (counter + 1)


if __name__ == '__main__':
    directory = sys.argv[1]
    movie_file = "movie.csv"
    actor_file = "actor.csv"
    director_file = "director.csv"
    cinema_file = "cinema.csv"
    award_file = "award.csv"
    actor_playedin_movie_file = "actor_playedin_movie.csv"
    movie_projectedin_cinema_file = "movie_projectedin_cinema.csv"
    director_supervised_movie_file = "director_supervised_movie.csv"
    movie_received_award_file = "movie_received_award.csv"

    counter = 0

    # clear any previous content
    for file in [movie_file, actor_file, director_file, award_file, actor_playedin_movie_file, movie_projectedin_cinema_file, director_supervised_movie_file, movie_received_award_file]:
        with open(os.path.join(directory, file), 'w') as dataset:
            dataset.write('')
            dataset.close()

    nb_movies = int(sys.argv[2])

    # generate k movies(_id, _labels, title, year, nbEntries, synopsis)
    # add between 3 and 6 actors per movie (store them to create the Actors CSV file later)
    # 1 movie director per movie
    # between 2 and 4 cinemas
    # between 0 and 3 awards
    movies = []
    actors = []
    cinemas = []
    movie_directors = []
    awards = []
    actor_playedin_movie = []
    movie_projectedin_cinema = []
    director_supervised_movie = []
    movie_received_award = []
    for i in range(nb_movies):
        isSequel = random.randint(0, 1)
        id, counter = create_id("Movie", counter)
        labels = ":Movie" # + (":Sequel" if isSequel else "")
        movie = [id, labels, random_string(random.randint(1, 8)), random_year(), random_large_int(), random_text(random.randint(10, 50)), random_large_int()]
        movies.append(movie)

        # generate from 3 to 6 actors.
        # at random, select an already existing actor or create a new actor
        nb_actors_in_movie = random.randint(3, 6)
        for j in range(nb_actors_in_movie):
            if random.randint(0,1) and len(actors) > 0:
                # select an existing actor
                # if there is no actor to select, create a new one
                actor = actors[random.randint(0, len(actors)-1)]
            else:
                # create a new actor
                id, counter = create_id("Actor", counter)
                labels = ":Actor:Person"
                actor = [id, labels, random_string(6), random_string(8), random_nationality()]
                actors.append(actor)
            actor_playedin_movie.append([actor[0], movie[0], ":playedIn", random_string(5)])

        # add one movie director (also, from the bank of directors, or a new one)
        if random.randint(0,1) and len(movie_directors) > 0:
            # select an existing movie director
            # if there is no actor to select, create a new one
            director = movie_directors[random.randint(0, len(movie_directors)-1)]
        else:
            # create a new movie director
            id, counter = create_id("Director", counter)
            labels = ":Director:Person"
            director = [id, labels, random_string(6), random_string(8)]
            movie_directors.append(director)
        director_supervised_movie.append([director[0], movie[0], ":supervised"])

        # add from 2 to 4 cinemas projecting the film (again, some cinemas have projected several movies, thus we selecta at random)
        nb_cinemas = random.randint(2, 4)
        for j in range(nb_cinemas):
            if random.randint(0,1) and len(cinemas) > 0:
                # select an existing cinema
                # if there is no cinema to select, create a new one
                cinema = cinemas[random.randint(0, len(cinemas)-1)]
            else:
                # create a new actor
                id, counter = create_id("Cinema", counter)
                labels = ":Cinema"
                cinema = [id, labels, random_string(6), random_nationality()]
                cinemas.append(cinema)
            movie_projectedin_cinema.append([movie[0], cinema[0], ":projectedIn", random_date()])

        # add from 0 to 3 awards
        nb_awards = random.randint(0, 3)
        for j in range(nb_awards):
            if random.randint(0,1) and len(awards) > 0:
                # select an existing award
                # if there is no award to select, create a new one
                award = awards[random.randint(0, len(awards)-1)]
            else:
                # create a new award
                id, counter = create_id("Award", counter)
                labels = ":Award"  # + (":Oscar" if random.randint(0, 1) else ":Cesar")
                award = [id, labels, random_string(6), random_year(), random.randint(0, 1)]
                awards.append(award)
            movie_received_award.append([movie[0], award[0], ":received"])


    # write movies
    df = pd.DataFrame(movies, columns=["_id", "_labels", "title", "year", "nbEntries", "synopsis", "duration"])
    df.to_csv(os.path.join(directory, movie_file), sep="|", index=False, quoting=1)

    # write actors
    df = pd.DataFrame(actors, columns=["_id", "_labels", "first_name", "last_name", "nationality"])
    df.to_csv(os.path.join(directory, actor_file), sep="|", index=False, quoting=1)

    # write movie directors
    df = pd.DataFrame(movie_directors, columns=["_id", "_labels", "first_name", "last_name"])
    df.to_csv(os.path.join(directory, director_file), sep="|", index=False, quoting=1)

    # write cinemas
    df = pd.DataFrame(cinemas, columns=["_id", "_labels", "name", "city"])
    df.to_csv(os.path.join(directory, cinema_file), sep="|", index=False, quoting=1)

    # write awards
    df = pd.DataFrame(awards, columns=["_id", "_labels", "label", "year", "is_international"])
    df.to_csv(os.path.join(directory, award_file), sep="|", index=False, quoting=1)

    # write actor_playedin_movie
    df = pd.DataFrame(actor_playedin_movie, columns=["_start", "_end", "_type", "role"])
    df.to_csv(os.path.join(directory, actor_playedin_movie_file), sep="|", index=False, quoting=1)

    # write movie_projectedin_cinema
    df = pd.DataFrame(movie_projectedin_cinema, columns=["_start", "_end", "_type", "date"])
    df.to_csv(os.path.join(directory, movie_projectedin_cinema_file), sep="|", index=False, quoting=1)

    # write director_supervised_movie
    df = pd.DataFrame(director_supervised_movie, columns=["_start", "_end", "_type"])
    df.to_csv(os.path.join(directory, director_supervised_movie_file), sep="|", index=False, quoting=1)

    # write movie_received_award
    df = pd.DataFrame(movie_received_award, columns=["_start", "_end", "_type"])
    df.to_csv(os.path.join(directory, movie_received_award_file), sep="|", index=False, quoting=1)

