import sys
import os
import pandas as pd

ENTITY_NODE_PREFIX = "http://abstra.fr/entityNode"
PROPERTY_NODE_PREFIX = "http://abstra.fr/propertyNode"
PROPERTY_PREFIX = "http://abstra.fr/"
TYPE_PREFIX = "http://abstra.fr/"
TYPE_OF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
COUNTER = 0


def sanitize_literal(literal):
    literal = str(literal.encode("utf-8", errors="ignore").decode().encode("ascii", errors="ignore").decode())
    literal = literal.replace("\"", "")
    literal = literal.replace("\\", "")
    return literal


if __name__ == '__main__':
    args = sys.argv
    print("Will convert the PG files located at " + args[1] + " into an RDF graph stored in the file " + args[2])

    # clear any previous content
    with open(args[2], 'w') as final_dataset:
        final_dataset.write('')
        final_dataset.close()

    with open(args[2], 'a') as final_dataset:
        # get the files in a list
        files = args[1].split(":")

        for f in files:
            if os.path.isfile(f) and f.endswith(".njn"):
                print(f)
                df = pd.read_csv(f, sep="|")
                df = df.astype(str)
                print(df.head())

                # we consider that the data is clean and formatted as expected
                # we convert the dataframe rows as a dict, i.e. key is the row index and value is the row
                # this seems to have a better scalability than iterrows() built-in function
                # see https://towardsdatascience.com/heres-the-most-efficient-way-to-iterate-through-your-pandas-dataframe-4dad88ac92ee
                df_dict = df.to_dict('records')

                print(df.columns)
                if "_id" not in df.columns or "_labels" not in df.columns:
                    print("Please be sure that your data is formatted as expected.")
                    break

                properties = df.columns.to_list()
                properties.remove('_id')
                properties.remove('_labels')

                for row in df_dict:
                    # print(row)
                    triples_for_row = ''
                    # we write the type(s) of the node
                    # of the form :Movie:Sequel. Can be empty if no typed
                    labels = row['_labels'].split(':')
                    for label in labels:
                        if label != '':
                            triples_for_row += '<' + ENTITY_NODE_PREFIX + str(row['_id']) + '> <' + TYPE_OF + '> <' + TYPE_PREFIX + label + '> .\n'
                    # we write its properties
                    for property in properties:
                        propertyValue = sanitize_literal(row[property])
                        triples_for_row += '<' + ENTITY_NODE_PREFIX + str(row['_id']) + '> <' + PROPERTY_PREFIX + str(property) + '> \"' + propertyValue + '\" .\n'
                    # finally, write the triples generate for that row into the file
                    final_dataset.write(triples_for_row)

            elif os.path.isfile(f) and f.endswith(".nje"):
                print(f)
                df = pd.read_csv(f, sep="|")
                df = df.astype(str)
                print(df.head())

                # we consider that the data is clean and formatted as expected
                # we convert the dataframe rows as a dict, i.e. key is the row index and value is the row
                # this seems to have a better scalability than iterrows() built-in function
                # see https://towardsdatascience.com/heres-the-most-efficient-way-to-iterate-through-your-pandas-dataframe-4dad88ac92ee
                df_dict = df.to_dict('records')

                if "_start" not in df.columns or "_type" not in df.columns:
                    # _end is facultative. An edge file may be used to map each node to several values (1:n)
                    # think of the relationship person -> speaks -> language where language is of the form "en", "fr", "es", ...
                    print("Please be sure that your data is formatted as expected.")
                    break

                edge_properties = df.columns.to_list()
                if "_start" in edge_properties:
                    edge_properties.remove("_start")
                if "_end" in edge_properties:
                    edge_properties.remove("_end")
                if "_type" in edge_properties:
                    edge_properties.remove("_type")

                for row in df_dict:
                    # print(row)
                    triples_for_row = ''

                    # get the edge label
                    edgeLabel = row["_type"]
                    if edgeLabel.startswith(":"):
                        edgeLabel = edgeLabel[1: len(edgeLabel)]

                    # if the edge has its proper properties, we create a node for it. Else we create a simple RDF triple
                    if len(edge_properties) == 0 or "_end" not in df.columns.to_list():
                        if "_end" in df.columns:
                            triples_for_row += '<' + ENTITY_NODE_PREFIX + str(row["_start"]) + '> <' + PROPERTY_PREFIX + edgeLabel + "> <" + ENTITY_NODE_PREFIX + row["_end"] + "> .\n"
                        else:
                            cols = df.columns.to_list()
                            cols.remove("_start")
                            cols.remove("_type")
                            # we take the last column as the target one
                            value = sanitize_literal(row[cols[0]])
                            triples_for_row += '<' + ENTITY_NODE_PREFIX + str(row["_start"]) + '> <' + PROPERTY_PREFIX + edgeLabel + "> \"" + value + "\" .\n"
                    else:
                        # create a (new) RDF node for the property
                        triples_for_row += '<' + PROPERTY_NODE_PREFIX + str(COUNTER) + '> <' + TYPE_OF + "> <" + TYPE_PREFIX + edgeLabel + "> .\n"

                        # link this new node to its source and target (existing) RDF nodes
                        # something of the form s --hasComment_subject--> P --hasComment_object--> o
                        triples_for_row += '<' + ENTITY_NODE_PREFIX + str(row["_start"]) + "> <" + PROPERTY_PREFIX + edgeLabel + "_subject" + "> <" + PROPERTY_NODE_PREFIX + str(COUNTER) + "> .\n"
                        triples_for_row += '<' + PROPERTY_NODE_PREFIX + str(COUNTER) + "> <" + PROPERTY_PREFIX + edgeLabel + "_object" + "> <" + ENTITY_NODE_PREFIX + str(row["_end"]) + "> .\n"

                        # attach to the property node its properties
                        for property in edge_properties:
                            propertyValue = sanitize_literal(row[property])
                            triples_for_row += "<" + PROPERTY_NODE_PREFIX + str(COUNTER) + "> <" + PROPERTY_PREFIX + property + "> \"" + propertyValue + "\" .\n"
                        COUNTER += 1
                    final_dataset.write(triples_for_row)
            else:
                raise IOError("Could not find the specified files. Please check their location.")
        final_dataset.close()
