import pandas as pd
import os
import sys

if __name__ == '__main__':
    # we assume that CSV nodes files are separated from relationship files.
    # something of the form:
    # data
    #  -- nodes
    #   -- entity1.csv
    #   -- entity2.csv
    # -- edges
    #   -- entity1_rel_entity2.csv
    filenameEdges = sys.argv[1] # 0 is the script filename
    path = filenameEdges[0:filenameEdges.rindex('/')] # rindex is the last index

    df = pd.read_csv(filenameEdges, sep=",")
    print(df.head())
    print(df.columns)
    df = df.rename({df.columns[0] : '_start'}, axis='columns')
    print(df.head())
    df = df.rename({df.columns[2] : "_end"}, axis='columns')
    print(df.head())
    df = df.rename({df.columns[1] : "_type"}, axis='columns')
    print(df.head())
    df = df.dropna(axis='columns', how = 'all')
    print(df.head())
    df = df.sort_values(by=['_type'])
    print(df.head())

    unique_values = df["_type"].unique().tolist()

    print(unique_values)

    for uniq_val in unique_values:
        df2 = df.loc[df['_type'] == uniq_val]
        uniq_val = uniq_val.replace("_", "")
        df2['_type'] = df2['_type'].str.replace("_", "") # remove _ in the relationship type (intermediary_of -> intermediaryof)
        print(path)
        print(os.path.join(path, "_"+uniq_val+"_.csv"))
        # ICIJ data do not have types, thus we are left with _rel_ (instead of A_rel_B)
        df2.to_csv(os.path.join(path, "_"+uniq_val+"_.csv"), sep="|", index=False, quoting=1) # quote all cells


    # NODES

    for filename in os.listdir(directoryNodes):
        f = os.path.join(directoryNodes, filename)

        if os.path.isfile(f) and f.endswith(".njn"):
            # get the node label
            filenameSplit = filename.split("_")
            nodeLabel = filenameSplit[0]

            df = pd.read_csv(f, sep="|")
            df = df.astype(str)

            # rename the id column with _id, create unique IDs based on the node label and the id, and add the label in a new columns
            if "_id" not in df.columns:
                df = df.rename({ "node_id" :"_id"}, axis='columns')
            df['_id'] = nodeLabel + df['_id'].astype(str) # concat the node label to the node id
            df['_labels'] = ":" + nodeLabel

            print(df.head())

            # write the changes back to the file
            df.to_csv(f, sep="|", index=False, quoting=1) # quote all cells and do not write line numbers (index) - but we write the header (columns)
