import pandas as pd
import os
import sys

if __name__ == '__main__':
    # we assume that CSV nodes files are separated from relationship files.
    # something of the form:
    # data
    #  -- nodes
    #   -- entity1.csv
    #   -- entity2.csv
    # -- edges
    #   -- entity1_rel_entity2.csv
    directoryNodes = sys.argv[1] # 0 is the script filename
    directoryEdges = sys.argv[2] # 0 is the script filename

    for filename in os.listdir(directoryNodes):
        f = os.path.join(directoryNodes, filename)

        if os.path.isfile(f) and f.endswith(".njn"):
            print(f)
            # get the node label
            filenameSplit = filename.split("_")
            nodeLabel = filenameSplit[0]

            df = pd.read_csv(f, sep="|")

            # rename the id column with _id, create unique IDs based on the node label and the id, and add the label in a new columns
            if "_id" not in df.columns:
                df.rename({ "id" :"_id"}, axis='columns', inplace=True)
                df['_id'] = nodeLabel + df['_id'].astype(str) # concat the node label to the node id
            df['_labels'] = ":" + nodeLabel
            if "creationDate" in df.columns:
                df.drop("creationDate", axis='columns', inplace=True) # if possible, drop the creationDate column

            print(df.head())

            # write the changes back to the file
            df.to_csv(f, sep="|", index=False, quoting=1) # quote all cells and do not write line numbers (index) - but we write the header (columns)

    for filename in os.listdir(directoryEdges):
        f = os.path.join(directoryEdges, filename)

        if os.path.isfile(f) and f.endswith(".nje"):
            print(f)
            # get the relationship label, with the source and target labels
            filenameSplit = filename.split("_")
            label_source = filenameSplit[0]
            relationship_label = filenameSplit[1]
            label_target = filenameSplit[2]

            df = pd.read_csv(f, sep="|")

            # rename the columns with _start and _end, create unique references with the node labels and add the relationship type
            first_column_with_id = -1
            second_column_with_id = -1
            counter = 0
            for column in df.columns:
                if "id" in column and first_column_with_id == -1:
                    first_column_with_id = counter
                elif "id" in column:
                    second_column_with_id = counter
                counter += 1

            if first_column_with_id > -1:
                df.rename({df.columns[first_column_with_id] : "_start"}, axis='columns', inplace=True)
                df["_start"] = label_source + df["_start"].astype(str)
            if second_column_with_id > -1:
                df.rename({df.columns[second_column_with_id] : "_end"}, axis='columns', inplace=True)
                df["_end"] = label_target + df["_end"].astype(str)
            df["_type"] = ":" + relationship_label
            if "creationDate" in df.columns:
                df.drop("creationDate", axis='columns', inplace=True) # if possible, drop the creationDate column

            print(df.head())

            # write the changes back to the file
            df.to_csv(f, sep="|", index=False, quoting=1) # quote all cells
