import sys
import re


# sanitize the rdf file `input_file` and writes the output in `output_file`
# call `sanitize_line` on each line of the file
def sanitize_file(input_file, output_file):
	print(input_file)
	fo = open(output_file, "w")
	with open(input_file, encoding='utf8') as fi:
		for line in fi:
			new_line = sanitize_line(line)
			fo.write(new_line)
			fo.write("\n")
	# fo.close()


# sanitize the given line by:
# - replacing unicode codes by their char,
# - removing escaped chars,
# - removing escaped single and double quotes
# - removing remaining backslashes
# - trim the spaces
def sanitize_line(line):
	# line = line.encode().decode('unicode-escape')  # interpret unicode characters before removing trailing backslashes
	line = line.encode("utf-8", errors="ignore").decode() 
	line = line.encode("ascii", errors="ignore").decode() # simply delete non-ascii characters
	line = line.replace('\\n', "").replace('\\r', "").replace('\\t', "")
	line = line.replace('\\"', "").replace("\\'", "'")  # deal with \" and \'
	line = line.replace('\\', "")
	line = line.replace('\"@en', '\"').replace('\"@fr', '\"')  # also remove annotations
	# remove remaining double quotes (because they break the string)
	index_occurrences = [_.start() for _ in re.finditer("\"", line)]
	index_occurrences = index_occurrences[
						1:-1]  # remove indexes for first and last quotes (because we don't want to remove them)
	iteration = 0
	for index in index_occurrences:
		line = line[:index - iteration] + line[(index - iteration + 1):]
		iteration += 1
	line = " ".join(line.split())  # simulate a trim
	return line


# removed the duplicates of the given file
# by loading all the lines into a set and writing back them in the output file
def remove_duplicates(input_file, output_file):
	unique_lines = set(open(input_file).readlines())
	with open(output_file, 'w') as fo:
		fo.writelines(unique_lines)


if __name__ == "__main__":
	args = sys.argv
	print("Sanitizing the RDF dataset '" + args[1] + "' and saving the output in '" + args[2] + "'")  # args[0] is the script filename
	sanitize_file(args[1], args[2])
	remove_duplicates(args[1], args[1])  # removes the duplicates in the file itself

