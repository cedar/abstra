# Introduction
Abstra is an **all-in-one dataset abstraction system** capable of creating the E-R schema (Entity-Relationship schema) of any RDF, JSON, XML or PG dataset. We call this task a _dataset abstraction_, or in short an _abstraction_.

Abstra has been developed on top of the [ConnectionLens project](https://gitlab.inria.fr/cedar/connectionlens). The repository is organized as follows:
* `core`: the core of the system
* `datasets`: the datasets used in the experimental section
* `evaluation`: the set of abstractions given to the evaluators 
* `evaluation_results`: the answers collected from the evaluation
* `experiments`: the set of scripts used to generate the experimentation section
* `scripts`: 
  * `data_preprocessing`: the Python files used to preprocess input data (notably RDF and PG)
  * `Flair_NER_tool`, `python` and `withSNERTokenizer`: the Python scripts used for the named entity extraction

Authors: Nelly Barret (Institut Polytechnique de Paris, France and Inria, France), Ioana Manolescu (Institut Polytechnique de Paris, France and Inria, France), Prajna Upadhyay (Inria, France).  
Please visit [our project website](https://team.inria.fr/cedar/projects/abstra/) for more details about the concepts and algorithms.

# Pre-requisites

You must have installed:
* [Postgres](https://www.postgresql.org/download/) (at least v9.6). A PostgreSQL server must be running locally, and you must have access to an account with the ability to create users. By default, the username is `abstra_user`, therefore this user must be created beforehand with `CREATE DATABASE` privileges.
* [Java](https://www.oracle.com/fr/java/technologies/javase/jdk11-archive-downloads.html) (v11 is recommended)
* [Python](https://www.python.org/downloads/) (v3.6 or v3.7 are recommended). Please note that your python3 installation should point to Python3.6 or Python3.7.
* [Dot](https://graphviz.org/download/)
<!--* [Tomcat](https://tomcat.apache.org/download-90.cgi) (at least v9)-->

# Abstra workflow

![Abstra workflow](abstra-schema.png)


# Getting started

## 1. Clone the repository
   
Clone the repository with `git clone https://gitlab.inria.fr/cedar/abstra.git`. 
     
## 2. Run the `configure` script

From the `abstra` directory, run the `configure` script as follows:
* **For Linux and MacOS users**, run `./configure.sh -d path/to/abstra/working/dir` 
* **For Windows users**, run `configure.bat path/to/abstra/working/dir` 

where `path/to/abstra/working/dir` is an _absolute path_ to a user-specified directory, e.g. `/Users/nbarret/Documents/abstra/abstra-working-dir/`.

**This will**:
* Create the (main) configuration file `local.settings` under `core/src/main/resources`.
* Copy all the resources that Abstra needs in `path/to/abstra/working/dir`.
* Be the location for Abstra descriptions and Abstra temporary files.

If needed, manually update the `RDBMS_user`, `RDBMS_password` and `RDBMS_port` properties with your Postgres user, password and port in the `local.settings` file present under `core/src/main/resources/`.
   
# Running an abstraction with the command-line

You can run the abstraction of a dataset with the default parameters by running **from the `abstra` directory** a command such as: 

`java -jar core/abstra-core-full-*.jar -i path/to/dataset -DRDBMS_DBName=abstra_db -c core/src/main/resources/local.settings`

This will load and abstract the input specified with the option `-i` into a database labelled `abstra_db` with the default abstraction parameters. The configuration file needs to be specified with `-c`.  
We recommend preprocessing (clean) RDF datasets before loading them using the dedicated script `preprocessRDFdata.py`. It is usable as follows: `python3 scripts/data_preprocessing/preprocessRDFdata.py /path/to/dataset.nt /path/to/sanitized-dataset.nt`. Note that datasets available in the `datasets` folder have already been preprocessed.

You can tune every abstraction parameter as follows:

| Parameter name | Description | Possible values | Default value |
|----------------|-------------|-----------------|---------------|
| `-sm` | Set the scoring method. | `DESC_k`, `LEAF_k`, `wDAG`, `wPR` or `wDWPR` where `k` is a number greater than 0 | `wDWPR` |
| `-bm` | Set the boundary method. | `DESC`, `LEAF`, `DAG`, `FL` or `FLAC` | `FLAC` |
| `-emax` | Set the maximal number of main collections | An integer k s.t. <ul><li>k >= 0 to report at most k main entities</li><li>k = -1 to report all the main collections until there are no more interesting collections to report</li></ul> | 5 |
| `-mtemax` | Set the maximal number of multi-traversed entities (added after the main collections selection) | An integer k s.t. <ul><li>k >= 0 to add at most k multi-traversed entities</li><li>k = -1 to add all the eligible multi-traversed entities</li></ul> | 3 |
| `-noattr` | Set whether collections without attributes are accepted during the reporting phase. | True or false | False |
| `-cmin` | Set the minimal coverage to reach before stopping the main collections' selection | A real number between 0 and 1 | 1 |
| `-etfmin` | Set the minimal edge transfer factor *etf* that an edge should have to be included in the boundary of a main collection | A real number between 0 and 1 | 0.8 |
| `-smin` | Set the minimal similarity between a semantic property and a data property | A real number between 0 and 1 | 0.8 |
| `-scmin` | tunes the minimal fitness between the collection properties and the assigned category | A real number between 0 and 1 | 0.3 |

If you want to only ingest the dataset as a graph (and not run the abstraction), use the parameter `-noabs` parameter.

Other parameters are available:
* `-n` is the "no-reset-at-start" parameter. When used, the database specified with the `DRDBMS_DBName` parameter will not be erased. It is helpful to run several abstractions without (re)loading the dataset at each run.
* `-cg` allows to start from the collection graph. This save the time of loading the dataset as a graph, normalize it and build the collection graph. This **should be used in conjunction with -n** and on a database where the collection graph has been built.
* `-v` is the parameter to set the verbose mode.
* `-log DEBUG` to also display DEBUG messages.

*Note for property graphs:*
- Node and edge files should be CSV-formatted with `|` delimiter and with values quoted to avoid any issues.
- Node files are expected to have the file extension `.njn`. Similarly, edge files are expected to have the file extension `.nje`.
- When abstracting a PG dataset, the `-i` parameter expects an input of the form `nodeA.njn:nodeB.njn:nodeN.njn:edgeAB.nje:edgeBC.nje:edgeNM.nje` (note the `:` to concatenate all the input files into one single dataset).

<!--
# Running an abstraction with the GUI

## Deploy the GUI 

1. Copy the `gui.war` archive in the Tomcat `webapps` folder (e.g. `/Applications/apache-tomcat-9.0.41/webapps/`) and unzip it there.
2. Copy your `local.settings` under `core/src/main/resources/` in `gui/WEB-INF/` in the deployed GUI folder.
3. Open a Web browser at the following address: `localhost:8080/gui/index.jsp`.


## GUI features

The main page offers three features:
* Create abstractions
* Read existing abstractions
* Visualize the graphs created for the abstractions

Help is provided for each feature in the interface with the "Help" buttons.
-->