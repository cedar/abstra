DATE=$1
COMMIT=$2
CL_JAR=../core/target/abstra-core-full-1.1-SNAPSHOT-*.jar
SETTINGS=../core/src/main/resources/local.settings
DATASETS=(../datasets/evaluation_rdf.nt ../datasets/evaluation_xml.xml ../datasets/evaluation_json.json ../datasets/pg/edges/post_hasTag_tag_0_0.nje:../datasets/pg/edges/person_studyAt_organisation_0_0.nje:../datasets/pg/edges/person_workAt_organisation_0_0.nje:../datasets/pg/edges/post_hasCreator_person_0_0.nje:../datasets/pg/edges/person_likes_comment_0_0.nje:../datasets/pg/edges/person_likes_post_0_0.nje:../datasets/pg/edges/forum_hasTag_tag_0_0.nje:../datasets/pg/edges/person_hasInterest_tag_0_0.nje:../datasets/pg/edges/person_knows_person_0_0.nje:../datasets/pg/edges/forum_hasModerator_person_0_0.nje:../datasets/pg/edges/forum_containerOf_post_0_0.nje:../datasets/pg/edges/forum_hasMember_person_0_0.nje:../datasets/pg/edges/comment_replyOf_post_0_0.nje:../datasets/pg/edges/comment_replyOf_comment_0_0.nje:../datasets/pg/edges/comment_hasTag_tag_0_0.nje:../datasets/pg/edges/comment_hasCreator_person_0_0.nje:../datasets/pg/nodes/tag_0_0.njn:../datasets/pg/nodes/person_0_0.njn:../datasets/pg/nodes/post_0_0.njn:../datasets/pg/nodes/forum_0_0.njn:../datasets/pg/nodes/organisation_0_0.njn:../datasets/pg/nodes/comment_0_0.njn)

# register each dataset only once
for dataset in ${DATASETS[@]}; do
  if [[ "$dataset" == *".njn"* ]]; then
    # PG dataset
    datasetName="evaluation_pg"
  else
    IFS='/' read -r -a split1 <<< "${dataset}"
    arraySize=${#split1[@]}
    datasetNameWithExt=${split1[arraySize-1]} # get the filename with its extension (bash arrays are zeo-indexed)
    IFS='.' read -r -a split2 <<< "${datasetNameWithExt}"
    datasetName=${split2[0]} # remove extension
  fi

   java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -i "${dataset}" -endat "collectionGraph" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-load.log

  # LEAF methods
  for sm in LEAF_1 LEAF_2 LEAF_3; do
    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm ${sm} -bm LEAF -endat "reporting" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-${sm}-LEAF.log
  done

  # DESC methods
  for sm in DESC_1 DESC_2 DESC_3; do
    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm ${sm} -bm DESC -endat "reporting" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-${sm}-DESC.log
  done

  # DAG method
  java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm wDAG -bm DAG -endat "reporting" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-wDAG-DAG.log

  # dw methods
  for sm in wPR wDWPR; do
    for bm in FL FLAC; do
      java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm ${sm} -bm ${bm} -endat "reporting" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-${sm}-${bm}.log
    done
  done
done

