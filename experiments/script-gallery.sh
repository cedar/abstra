
##################
## Think of the default parameters in the local.settings: extractor, path_to_dot
##################

DATE=2023-01-31
COMMIT=2870e0843f76352549545f74ed7ae54d543cd364
CL_JAR=../core/target/abstra-core-full-1.1-SNAPSHOT-*.jar
SETTINGS=../core/src/main/resources/local.settings
#WORKING_DIR=/local/nbarret/abstraction-work/abstra-working-dir/tmp
WORKING_DIR=/Users/nelly/Documents/boulot/theseNelly/abstraction-work/abstra-working-dir/tmp
DATASETS=(../datasets/bsbm1m.nt ../datasets/clean_energy.nt ../datasets/conferences.nt ../datasets/enelshops.nt ../datasets/foodista.nt ../datasets/lubm1m.nt ../datasets/nasa.nt ../datasets/mondial.xml ./datasets/xmark025.xml) # those are the ones that I can do on my own laptop
 #../datasets/coreresearch.json ../datasets/github.json ../datasets/nytimes.json ../datasets/prescriptions.json ../datasets/pubmed.xml ../datasets/researchers10k.json ../datasets/wikimedia.xml ../datasets/yelpbusiness.json ../datasets/yelpcheckin.json ../datasets/yelptip.json)
 # ../core/data/neo4j/ldbc-small/Post.njn:../core/data/neo4j/ldbc-small/Post_isLocatedIn_Country.nje:../core/data/neo4j/ldbc-small/Person.njn:../core/data/neo4j/ldbc-small/Post_hasCreator_Person.nje:../core/data/neo4j/ldbc-small/Post_hasTag_Tag.nje:../core/data/neo4j/ldbc-small/Person_studyAt_University.nje:../core/data/neo4j/ldbc-small/Person_workAt_Company.nje:../core/data/neo4j/ldbc-small/Person_knows_Person.nje:../core/data/neo4j/ldbc-small/Person_likes_Comment.nje:../core/data/neo4j/ldbc-small/Person_likes_Post.nje:../core/data/neo4j/ldbc-small/Person_hasInterest_Tag.nje:../core/data/neo4j/ldbc-small/Person_isLocatedIn_City.nje:../core/data/neo4j/ldbc-small/Forum.njn:../core/data/neo4j/ldbc-small/Forum_hasTag_Tag.nje:../core/data/neo4j/ldbc-small/Forum_hasModerator_Person.nje:../core/data/neo4j/ldbc-small/Forum_hasMember_Person.nje:../core/data/neo4j/ldbc-small/Forum_containerOf_Post.nje:../core/data/neo4j/ldbc-small/Comment.njn:../core/data/neo4j/ldbc-small/Comment_replyOf_Post.nje:../core/data/neo4j/ldbc-small/Comment_replyOf_Comment.nje:../core/data/neo4j/ldbc-small/Comment_isLocatedIn_Country.nje:../core/data/neo4j/ldbc-small/Comment_hasTag_Tag.nje:../core/data/neo4j/ldbc-small/Comment_hasCreator_Person.nje ../core/data/neo4j/ldbc-small-no-dates/Post.njn:../core/data/neo4j/ldbc-small-no-dates/Post_isLocatedIn_Country.nje:../core/data/neo4j/ldbc-small-no-dates/Person.njn:../core/data/neo4j/ldbc-small-no-dates/Post_hasCreator_Person.nje:../core/data/neo4j/ldbc-small-no-dates/Post_hasTag_Tag.nje:../core/data/neo4j/ldbc-small-no-dates/Person_studyAt_University.nje:../core/data/neo4j/ldbc-small-no-dates/Person_workAt_Company.nje:../core/data/neo4j/ldbc-small-no-dates/Person_knows_Person.nje:../core/data/neo4j/ldbc-small-no-dates/Person_likes_Comment.nje:../core/data/neo4j/ldbc-small-no-dates/Person_likes_Post.nje:../core/data/neo4j/ldbc-small-no-dates/Person_hasInterest_Tag.nje:../core/data/neo4j/ldbc-small-no-dates/Person_isLocatedIn_City.nje:../core/data/neo4j/ldbc-small-no-dates/Forum.njn:../core/data/neo4j/ldbc-small-no-dates/Forum_hasTag_Tag.nje:../core/data/neo4j/ldbc-small-no-dates/Forum_hasModerator_Person.nje:../core/data/neo4j/ldbc-small-no-dates/Forum_hasMember_Person.nje:../core/data/neo4j/ldbc-small-no-dates/Forum_containerOf_Post.nje:../core/data/neo4j/ldbc-small-no-dates/Comment.njn:../core/data/neo4j/ldbc-small-no-dates/Comment_replyOf_Post.nje:../core/data/neo4j/ldbc-small-no-dates/Comment_replyOf_Comment.nje:../core/data/neo4j/ldbc-small-no-dates/Comment_isLocatedIn_Country.nje:../core/data/neo4j/ldbc-small-no-dates/Comment_hasTag_Tag.nje:../core/data/neo4j/ldbc-small-no-dates/Comment_hasCreator_Person.nje

for dataset in ${DATASETS[@]}; do
  # extract the dataset name
  IFS='/' read -r -a split1 <<< "${dataset}"
  arraySize=${#split1[@]}
  datasetNameWithExt=${split1[arraySize-1]} # get the filename with its extension (bash arrays are zeo-indexed)
  IFS='.' read -r -a split2 <<< "${datasetNameWithExt}"
  datasetName=${split2[0]} # remove extension

  # load the dataset
   echo "loading dataset ${datasetName} on path ${dataset}"
#   /usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -i "${dataset}" -endat "collectionGraph" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a /local/nbarret/${DATE}/${datasetName}-load.log
   java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -i "${dataset}" -endat "collectionGraph" -idref "ENABLE_SCORE" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-load.log

  for sm in wDAG; do
    for bm in DAG; do
      # make sure the database is clean with a VACUUM
      # -z to also ANALYZE (generate the stats)
      # --full to do a full vacuum
      # vacuumdb requires an exclusive lock, thus we kill any running query
#      psql -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328 -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();"
#      vacuumdb abstra_${datasetName} -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328 -z --full
#      psql -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328 -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();"
      psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();"
      vacuumdb abstra_${datasetName} -z --full
      psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();"
#
#      # do its abstraction(s)
      echo "doing abstraction of ${datasetNameWithExt} with parameters ${sm} - ${bm}"
##      /usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm ${sm} -bm ${bm} -endat "reporting" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-${sm}-${bm}.log
      java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_${datasetName} -n -cg -sm ${sm} -bm ${bm} -endat "reporting" -v -c ${SETTINGS} 2>&1 | tee -a ${DATE}/${datasetName}-${sm}-${bm}.log
#
#      # copy the generated E-R schema in the repo
      cp $WORKING_DIR/ERSchema_${datasetName}_${sm}_${bm}_5_1.0_0.8_0.8_0.3.png ../gallery/
    done
  done
done
