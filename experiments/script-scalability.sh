DATE=$1
COMMIT=$2
CL_JAR=../core/target/abstra-core-full-1.1-SNAPSHOT-*.jar
FOLDER_DATA=/data/datasets/abstraction_data
SETTINGS=../core/src/main/resources/local.settings

sm=wDWPR
bm=FL

# JSON

#for size in 10 20 40 80 160; do
#	echo "doing data loading of researchers${size}k - with parameters ${sm} - ${bm}"
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_researchers${size}k -i ${FOLDER_DATA}/researchers${size}k.json -as -endat collectionGraph -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/researchers${size}k-${COMMIT}-load.log
#	psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "VACUUM FULL ANALYZE abstra_researchers${size}k" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#	echo "doing abstraction of researchers${size}k - with parameters ${sm} - ${bm}"
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_researchers${size}k -n -cg -sm ${sm} -bm ${bm} -as -v -c ${SETTINGS} > /local/nbarret/${DATE}/researchers${size}k-${COMMIT}-${sm}_${bm}.log;
#done

# XML

#for size in 025 05 1 2 4; do
#	echo "doing data loading of xmark${size} - with parameters ${sm} - ${bm}";
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark${size} -i ${FOLDER_DATA}/xmark${size}.xml -as -endat collectionGraph -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/xmark${size}-${COMMIT}-load.log;
#	psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "VACUUM FULL ANALYZE abstra_xmark${size}" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  echo "doing abstraction of xmark${size} - with parameters ${sm} - ${bm}"
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark${size} -n -cg -sm ${sm} -bm ${bm} -as -v -c ${SETTINGS} > /local/nbarret/${DATE}/xmark${size}-${COMMIT}-${sm}_${bm}.log;
#done


# RDF

#for size in 1 2 4 8 16; do
#	echo "doing data loading of bsbm${size}m - with parameters ${sm} - ${bm}";
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_bsbm${size}m -i ${FOLDER_DATA}/bsbm${size}m.nt -as -endat collectionGraph -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/bsbm${size}m-${COMMIT}-load.log;
#	psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "VACUUM FULL ANALYZE abstra_bsbm${size}m" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  psql -c "SELECT pg_cancel_backend(pid) FROM pg_stat_activity WHERE state = 'active' and pid <> pg_backend_pid();" -h /data/nbarret/psql-data-cedar004/ -U nbarret -p 54328
#  echo "doing abstraction of bsbm${size}m - with parameters ${sm} - ${bm}"
#	/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_bsbm${size}m -n -cg -sm ${sm} -bm ${bm} -as -v -c ${SETTINGS} > /local/nbarret/${DATE}/bsbm${size}m-${COMMIT}-${sm}_${bm}.log;
#done


# PG

echo "doing abstraction of movies15K - with parameters ${sm} - ${bm}";
/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_movies15K -i /data/datasets/abstraction_data/movies15K/edges/movie_received_award.nje:/data/datasets/abstraction_data/movies15K/edges/movie_projectedin_cinema.nje:/data/datasets/abstraction_data/movies15K/edges/director_supervised_movie.nje:/data/datasets/abstraction_data/movies15K/edges/actor_playedin_movie.nje:/data/datasets/abstraction_data/movies15K/nodes/movie.njn:/data/datasets/abstraction_data/movies15K/nodes/director.njn:/data/datasets/abstraction_data/movies15K/nodes/cinema.njn:/data/datasets/abstraction_data/movies15K/nodes/award.njn:/data/datasets/abstraction_data/movies15K/nodes/actor.njn -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/movies15K-${COMMIT}-${sm}-${bm}.log;

echo "doing abstraction of movies30k - with parameters ${sm} - ${bm}";
/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_movies30k -i /data/datasets/abstraction_data/movies30K/edges/movie_received_award.nje:/data/datasets/abstraction_data/movies30K/edges/movie_projectedin_cinema.nje:/data/datasets/abstraction_data/movies30K/edges/director_supervised_movie.nje:/data/datasets/abstraction_data/movies30K/edges/actor_playedin_movie.nje:/data/datasets/abstraction_data/movies30K/nodes/movie.njn:/data/datasets/abstraction_data/movies30K/nodes/director.njn:/data/datasets/abstraction_data/movies30K/nodes/cinema.njn:/data/datasets/abstraction_data/movies30K/nodes/award.njn:/data/datasets/abstraction_data/movies30K/nodes/actor.njn -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/movies30K-${COMMIT}-${sm}-${bm}.log;

echo "doing abstraction of movies60k - with parameters ${sm} - ${bm}";
/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_movies60k -i /data/datasets/abstraction_data/movies60K/edges/movie_received_award.nje:/data/datasets/abstraction_data/movies60K/edges/movie_projectedin_cinema.nje:/data/datasets/abstraction_data/movies60K/edges/director_supervised_movie.nje:/data/datasets/abstraction_data/movies60K/edges/actor_playedin_movie.nje:/data/datasets/abstraction_data/movies60K/nodes/movie.njn:/data/datasets/abstraction_data/movies60K/nodes/director.njn:/data/datasets/abstraction_data/movies60K/nodes/cinema.njn:/data/datasets/abstraction_data/movies60K/nodes/award.njn:/data/datasets/abstraction_data/movies60K/nodes/actor.njn -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/movies60k-${COMMIT}-${sm}-${bm}.log;

echo "doing abstraction of movies125k - with parameters ${sm} - ${bm}";
/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_movies125k -i /data/datasets/abstraction_data/movies125K/edges/movie_received_award.nje:/data/datasets/abstraction_data/movies125K/edges/movie_projectedin_cinema.nje:/data/datasets/abstraction_data/movies125K/edges/director_supervised_movie.nje:/data/datasets/abstraction_data/movies125K/edges/actor_playedin_movie.nje:/data/datasets/abstraction_data/movies125K/nodes/movie.njn:/data/datasets/abstraction_data/movies125K/nodes/director.njn:/data/datasets/abstraction_data/movies125K/nodes/cinema.njn:/data/datasets/abstraction_data/movies125K/nodes/award.njn:/data/datasets/abstraction_data/movies125K/nodes/actor.njn -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/movies125k-${COMMIT}-${sm}-${bm}.log;

echo "doing abstraction of movies250k - with parameters ${sm} - ${bm}";
/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_movies250k -i /data/datasets/abstraction_data/movies250K/edges/movie_received_award.nje:/data/datasets/abstraction_data/movies250K/edges/movie_projectedin_cinema.nje:/data/datasets/abstraction_data/movies250K/edges/director_supervised_movie.nje:/data/datasets/abstraction_data/movies250K/edges/actor_playedin_movie.nje:/data/datasets/abstraction_data/movies250K/nodes/movie.njn:/data/datasets/abstraction_data/movies250K/nodes/director.njn:/data/datasets/abstraction_data/movies250K/nodes/cinema.njn:/data/datasets/abstraction_data/movies250K/nodes/award.njn:/data/datasets/abstraction_data/movies250K/nodes/actor.njn -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/movies250k-${COMMIT}-${sm}-${bm}.log;


#echo "doing abstraction of ldbc01 - with parameters ${sm} - ${bm}";
#/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_ldbc01 -i /data/datasets/abstraction_data/ldbc0.1/nodes/post_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/comment_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/forum_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/organisation_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/person_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/place_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/tag_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/nodes/tagclass_0_0.njn:/data/datasets/abstraction_data/ldbc0.1/edges/forum_hasModerator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/comment_hasCreator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/comment_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/comment_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/comment_replyOf_comment_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/comment_replyOf_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/forum_containerOf_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/forum_hasMember_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/forum_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/organisation_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_email_emailaddress_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_hasInterest_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_knows_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_likes_comment_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_likes_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_speaks_language_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_studyAt_organisation_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/person_workAt_organisation_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/place_isPartOf_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/post_hasCreator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/post_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/post_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/tag_hasType_tagclass_0_0.nje:/data/datasets/abstraction_data/ldbc0.1/edges/tagclass_isSubclassOf_tagclass_0_0.nje -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/ldbc01-${COMMIT}-${sm}-${bm}.log;

#echo "doing abstraction of ldbc03 - with parameters ${sm} - ${bm}";
#/usr/lib/jvm/java-11-openjdk-11.0.14.1.1-1.el7_9.x86_64/bin/java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_ldbc03 -i /data/datasets/abstraction_data/ldbc0.3/nodes/post_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/comment_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/forum_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/organisation_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/person_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/place_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/tag_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/nodes/tagclass_0_0.njn:/data/datasets/abstraction_data/ldbc0.3/edges/forum_hasModerator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/comment_hasCreator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/comment_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/comment_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/comment_replyOf_comment_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/comment_replyOf_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/forum_containerOf_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/forum_hasMember_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/forum_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/organisation_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_email_emailaddress_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_hasInterest_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_knows_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_likes_comment_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_likes_post_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_speaks_language_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_studyAt_organisation_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/person_workAt_organisation_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/place_isPartOf_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/post_hasCreator_person_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/post_hasTag_tag_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/post_isLocatedIn_place_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/tag_hasType_tagclass_0_0.nje:/data/datasets/abstraction_data/ldbc0.3/edges/tagclass_isSubclassOf_tagclass_0_0.nje -as -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > /local/nbarret/${DATE}/ldbc03-${COMMIT}-${sm}-${bm}.log;

