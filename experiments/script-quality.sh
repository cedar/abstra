DATE=2022-10-14
COMMIT=1b10acb82d2512e32ea9946ec33b9de4f92d7b7c
CL_JAR=core/target/abstra-core-1.1-SNAPSHOT-*.jar
FOLDER_DATA=/data/datasets/abstraction_data
SETTINGS=core/src/main/resources/local.settings

for smVal in wDAG
do
  for bmVal in DAG FL FLAC
  do
    sm=${smVal}
    
    bm=${bmVal}

#    echo "doing abstraction of core dataset - with data loading and parameters ${sm} - ${bm}"
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_core -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/core-${COMMIT}-${sm}_${bm}.log;
#
    echo "doing abstraction of github events - with data loading and parameters ${sm} - ${bm}";
    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_github -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/github-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of NYtimes - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_nytimes -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/nytimes-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of JSON prescriptions - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_prescriptions -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/prescriptions-${COMMIT}-${sm}_${bm}.log;

    echo "doing abstraction of researchers160k - with data loading and parameters ${sm} - ${bm}";
    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_researchers160k -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/researchers160k-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of yelp businesses - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_yelpbusiness -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/yelpbusiness-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of yelp checkin - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_yelpcheckin -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/yelpcheckin-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of yelp tips - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_yelptip -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/yelptip-${COMMIT}-${sm}_${bm}.log;

    echo "doing abstraction of bsbm4m - with data loading and parameters ${sm} - ${bm}";
    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_bsbm4m -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/bsbm4m-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of bsbm16m - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_bsbm16m -i ${FOLDER_DATA}/bsbm16m.nt -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/bsbm16m-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of conferences - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_conferences -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/conferences-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of enelshops - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_enelshops -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/enelshops-${COMMIT}-${sm}_${bm}.log;
#
#    echo "doing abstraction of foodista - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_foodista -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/foodista-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of lubm1m - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_lubm1m -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/lubm1m-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of nasa - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_nasa -i ${FOLDER_DATA}/nasa.nt -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/nasa-${COMMIT}-${sm}_${bm}.log;

#    echo "doing abstraction of pubmed data - with data loading and parameters ${sm} - ${bm}";
#    java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_pubmed -n -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/pubmed-${COMMIT}-${sm}_${bm}.log;

    #echo "doing abstraction of xmark4 - with data loading and parameters ${sm} - ${bm}";
    #java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark4 -i ${FOLDER_DATA}/xmark4.xml -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/xmark4-${COMMIT}-${sm}_${bm}.log;

  done
done

echo "doing abstraction of xmark1 - with data loading and parameters -sm wDAG -bm DAG";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -i ${FOLDER_DATA}/xmark1.xml -abs -sm wDAG -bm DAG -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "doing abstraction of xmark1 - with data loading and parameters -sm wDAG -bm FL";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -n -abs -sm wDAG -bm FL -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "doing abstraction of xmark1 - with data loading and parameters -sm wDAG -bm FLAC";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -n -abs -sm wDAG -bm FLAC -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "doing abstraction of xmark1 - with data loading and parameters -sm wPR -bm DAG";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -n -abs -sm wPR -bm DAG -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "doing abstraction of xmark1 - with data loading and parameters -sm wPR -bm FL";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -n -abs -sm wPR -bm FL -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "doing abstraction of xmark1 - with data loading and parameters -sm wPR -bm FLAC";
java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_xmark1 -n -abs -sm wPR -bm FLAC -v -c ${SETTINGS} > ../${DATE}/xmark1-${COMMIT}-${sm}_${bm}.log;

echo "finished all abstractions";

################

#echo "doing abstraction of watdiv4 - with data loading and parameters ${sm} - ${bm}";
#java -Xmx80g -jar ${CL_JAR} -DRDBMS_DBName=abstra_watdiv4 -i ${FOLDER_DATA}/watdiv4.nt -abs -sm ${sm} -bm ${bm} -v -c ${SETTINGS} > ../${DATE}/watdiv4-${COMMIT}-${sm}_${bm}.log;

