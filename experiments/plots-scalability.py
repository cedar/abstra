import matplotlib.pyplot as plt
import numpy as np

def plot_chart(nb_nodes, time_normalization, time_collectiongraph, time_main_collections, time_classification, dataset_name, nb_nodes_min, nb_nodes_max):
    font_size = 17
    fig, ax = plt.subplots()
    scale = 1000
    ax.plot(nb_nodes, np.divide(np.array(time_normalization), scale), label="normalization", marker='o')
    ax.plot(nb_nodes, np.divide(np.array(time_collectiongraph), scale), label="collection graph building", marker='^')
    ax.plot(nb_nodes, np.divide(np.array(time_main_collections), scale), label="main collections selection", marker='s')
    ax.plot(nb_nodes, np.divide(np.array(time_classification), scale), label="classification", marker='D')
    # ax.plot(nb_nodes, np.divide(np.array(time_reporting), scale), label="reporting", linestyle=":", marker='o')
    plt.xlabel("Number of nodes \n (min=" + str(f'{nb_nodes_min:,}') + ", max=" + str(f'{nb_nodes_max:,}') + ")", fontsize=font_size)
    plt.ylabel("Time (in seconds)", fontsize=font_size)
    plt.xscale("log")
    plt.yscale("log")
    plt.legend(loc="lower right", fontsize=0.7*font_size)
    ax.text(.5, .93, "Abstraction time for " + str(dataset_name) + " datasets", horizontalalignment='center', transform=ax.transAxes, fontsize=font_size)  # set the title in the drawing
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    # plt.show()
    plt.tight_layout(rect=(0, 0, 1, 1))
    plt.savefig("chart_" + dataset_name+".png")


if __name__ == "__main__":
    # scalability expes JSON
    nb_nodes = [279755, 560345, 1122323, 2240989, 4505657]
    time_normalization = [4294, 9119, 18977, 45307, 80502]
    time_collectiongraph = [4740, 13644, 23887, 50590, 109527]
    time_main_collections = [20140, 43264, 84437, 199967, 511031]
    time_classification = [26117, 26832, 35817, 59653, 134685]

    plot_chart(nb_nodes, time_normalization, time_collectiongraph, time_main_collections, time_classification, "Researchers", min(nb_nodes), max(nb_nodes))

    # NOTE: count on nodes, not norm_nodes

    # scalability expes XMark
    nb_nodes = [848988, 1695790, 3392392, 6795209, 13615551]
    time_normalization = [15221, 32329, 71545, 146820, 272526]
    time_collectiongraph = [43959, 82519, 258927, 542194, 1288559]
    time_main_collections = [136625, 279965, 582487, 1154410, 2780715]
    time_classification = [63572, 75856, 104516, 162086, 280675]
    plot_chart(nb_nodes, time_normalization, time_collectiongraph, time_main_collections, time_classification, "XMark", min(nb_nodes), max(nb_nodes))

    # scalability expes BSBM
    nb_nodes = [295597, 585787, 1134834, 2219177, 5344748]
    time_normalization = [109852, 224718, 454709, 969616, 1904536]
    time_collectiongraph = [50655, 109261, 210434, 378108, 830558]
    time_main_collections = [211983, 417916, 1111924, 1892958, 5036639]
    time_classification = [27554, 51758, 118453, 265841, 762090]
    plot_chart(nb_nodes, time_normalization, time_collectiongraph, time_main_collections, time_classification, "BSBM", min(nb_nodes), max(nb_nodes))

    # scalability expes Movies
    nb_nodes = [467541, 914729, 1804650, 3720160, 12386250]
    time_normalization = [58477, 119462, 245093, 534562, 1260937]
    time_collectiongraph = [156147, 342689, 522808, 1306368, 3555345]
    time_main_collections = [152845, 318805, 653841, 1751345, 6162217]
    time_classification = [15997, 31554, 59956, 132458, 640628]
    plot_chart(nb_nodes, time_normalization, time_collectiongraph, time_main_collections, time_classification, "Movies", min(nb_nodes), max(nb_nodes))
