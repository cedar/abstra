#!/bin/bash

usage() {
  echo "Usage: ${0} -d <path/to/Abstra/working/directory> -t <path/to/deployed/app/tin/tomcat>"
  echo ""
  echo "    -d: <path/to/Abstra/working/directory>. The path to the directory that we will used as a working directory for Abstra (copying models and writing some temporary files)."
  # echo "    -t: <path/to/deployed/app/tin/tomcat>. The absolute path of the deployed Abstra GUI in Tomcat. Can be set to # if no app is deployed. "
  exit 0
}

realpath() {
  OURPWD=$PWD
  cd "$(dirname "$1")"
  LINK=$(readlink "$(basename "$1")")
  while [ "$LINK" ]; do
    cd "$(dirname "$LINK")"
    LINK=$(readlink "$(basename "$1")")
  done
  REALPATH="$PWD/$(basename "$1")"
  cd "$OURPWD"
  echo "$REALPATH"
}

# check that each argument exists
# if yes, set the variable with the parameter value
while getopts "d:t:" opt; do
  if [ -z ${OPTARG} ]
  then
    usage
  else
    case "${opt}" in
      d)
        ABSTRA_DIR=${OPTARG}
        ;;
      # t)
      #  APP_DIR=${OPTARG}
      #  ;;
      *) usage;;
    esac
  fi
done

if [[ -z ${ABSTRA_DIR} ]] # || [ -z ${APP_DIR} ]
then
  usage
else
  echo "ABSTRA_DIR is :'$ABSTRA_DIR'"
  # echo "APP_DIR is :'$APP_DIR'"
fi

ROOT=core/lib
CACHE_DIR=cache
TMP_DIR=tmp
TT_DIR=treetagger
MODELS_DIR=models
CMD_DIR=cmd
LIB_DIR=lib
SCRIPTS_DIR=scripts
HEIDELTIME_DIR=heideltime
CLASSIFICATION_DIR=classification
REPORTING_DIR=reporting

# get OS to get the relevant executables for treetagger
TT_BIN=""
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux OS
  TT_BIN=binLinux;
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  TT_BIN=binMacos
else
  # by default, we will try with Linux
  TT_BIN=binLinux
fi

ABSTRA_TT=${ROOT}/${TT_DIR}
ABSTRA_TT_BIN=${ABSTRA_TT}/${TT_BIN}
ABSTRA_TT_CMD=${ABSTRA_TT}/${CMD_DIR}
ABSTRA_TT_LIB=${ABSTRA_TT}/${LIB_DIR}
ABSTRA_TT_MODELS=${ABSTRA_TT}/${MODELS_DIR}
ABSTRA_HEIDELTIME=${ROOT}/${HEIDELTIME_DIR}
ABSTRA_SCRIPTS=${SCRIPTS_DIR}
ABSTRA_CLASSIFICATION=${ROOT}/${CLASSIFICATION_DIR}
ABSTRA_REPORTING=${ROOT}/${REPORTING_DIR}

LOCAL_CACHE=${ABSTRA_DIR}/${CACHE_DIR}
LOCAL_TMP=${ABSTRA_DIR}/${TMP_DIR}
LOCAL_TT=${ABSTRA_DIR}/${TT_DIR}
LOCAL_TT_BIN=${LOCAL_TT}/bin
LOCAL_TT_CMD=${LOCAL_TT}/${CMD_DIR}
LOCAL_TT_LIB=${LOCAL_TT}/${LIB_DIR}
LOCAL_TT_MODELS=${LOCAL_TT}/${MODELS_DIR}
LOCAL_HEIDELTIME=${ABSTRA_DIR}/${HEIDELTIME_DIR}
LOCAL_SCRIPTS=${ABSTRA_DIR}/${SCRIPTS_DIR}
LOCAL_CLASSIFICATION=${ABSTRA_DIR}/${CLASSIFICATION_DIR}
LOCAL_REPORTING=${ABSTRA_DIR}/${REPORTING_DIR}
LOCAL_PYTHON_ENV=${ABSTRA_DIR}/abstra_env
LOCAL_SETTINGS=${ABSTRA_DIR}/local.settings


# create folders
mkdir -p $ABSTRA_DIR
mkdir -p $LOCAL_CACHE
mkdir -p $LOCAL_TMP
mkdir -p $LOCAL_TT
mkdir -p $LOCAL_TT_BIN
mkdir -p $LOCAL_TT_CMD
mkdir -p $LOCAL_TT_LIB
mkdir -p $LOCAL_TT_MODELS
mkdir -p $LOCAL_HEIDELTIME
mkdir -p $LOCAL_SCRIPTS
mkdir -p $LOCAL_CLASSIFICATION
mkdir -p $LOCAL_REPORTING

# copy necessary files
cp -R ${ABSTRA_TT_BIN}/. ${LOCAL_TT_BIN}/.
cp -R ${ABSTRA_TT_CMD}/. ${LOCAL_TT_CMD}/.
cp -R ${ABSTRA_TT_LIB}/. ${LOCAL_TT_LIB}/.
cp -R ${ABSTRA_TT_MODELS}/. ${LOCAL_TT_MODELS}/.
cp -R ${ABSTRA_SCRIPTS}/. ${LOCAL_SCRIPTS}/.
cp -R ${ABSTRA_HEIDELTIME}/. ${LOCAL_HEIDELTIME}/.
cp -R ${ABSTRA_CLASSIFICATION}/. ${LOCAL_CLASSIFICATION}/.
cp -R ${ABSTRA_REPORTING}/. ${LOCAL_REPORTING}/.

cp core/lib/RDFQuotient-2.2-with-dependencies.jar $ABSTRA_DIR/.
cp core/src/main/resources/parameter.settings $ABSTRA_DIR/local.settings


# configure Python venv
# Mac OSX or Linux OS
python3 -m venv ${LOCAL_PYTHON_ENV}
source ${LOCAL_PYTHON_ENV}/bin/activate
python3 -m pip install --upgrade pip
pip3 install -r requirements.txt
deactivate


# add to local.settings the  absolute path to models
printf "\n\n\n#### GENERATED PARAMETERS\n\n" >> ${LOCAL_SETTINGS}

printf "# Temporary directory for Abstra\n" >> ${LOCAL_SETTINGS}
printf "temp_dir=${LOCAL_TMP}\n\n" >> ${LOCAL_SETTINGS}

#printf "# Tomcat directory for Abstra GUI\n" >> ${LOCAL_SETTINGS}
#printf "tomcat_dir=${APP_DIR}\n\n" >> ${LOCAL_SETTINGS}

printf "# Abstra cache location\n" >> ${LOCAL_SETTINGS}
printf "cache_location=${LOCAL_CACHE}\n\n" >> ${LOCAL_SETTINGS}

printf "# Python location\n" >> ${LOCAL_SETTINGS}
printf "python_path=${LOCAL_PYTHON_ENV}/bin/python\n\n" >> ${LOCAL_SETTINGS}
printf "# Path to python scripts (FLAIR extraction)\n" >> ${LOCAL_SETTINGS}
printf "python_script_location=${LOCAL_SCRIPTS}\n\n" >> ${LOCAL_SETTINGS}
printf "data_processing_script_location=${LOCAL_SCRIPTS}/data_preprocessing\n\n" >> ${LOCAL_SETTINGS}
printf "# Treetagger location\n" >> ${LOCAL_SETTINGS}
printf "treetagger_home=${LOCAL_TT}\n\n" >> ${LOCAL_SETTINGS}
printf "# Stanford models location\n" >> ${LOCAL_SETTINGS}
printf "stanford_models=${LOCAL_TT_MODELS}\n\n" >> ${LOCAL_SETTINGS}
printf "# Configuration file used by HeidelTime (date extractor)\n" >> ${LOCAL_SETTINGS}
printf "config_heideltime=${LOCAL_HEIDELTIME}/config-heideltime.props\n\n" >> ${LOCAL_SETTINGS}

printf "# Word2Vec model location (used for classification during abstraction)\n" >> ${LOCAL_SETTINGS}
printf "word_embedding_model_path=${LOCAL_CLASSIFICATION}/word2vec.bin\n" >> ${LOCAL_SETTINGS}
printf "stop_words_english=${LOCAL_CLASSIFICATION}/stop_words_english.txt\n" >> ${LOCAL_SETTINGS}
printf "stop_words_french=${LOCAL_CLASSIFICATION}/stop_words_french.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# set of manually-defined semantic properties\n" >> ${LOCAL_SETTINGS}
printf "set_of_semantic_properties_manual=${LOCAL_CLASSIFICATION}/set-of-semantic-properties-manual.json\n\n" >> ${LOCAL_SETTINGS}
printf "# set of semantic properties extended with Yago and DBPedia triples\n" >> ${LOCAL_SETTINGS}
printf "set_of_semantic_properties_extended=${LOCAL_CLASSIFICATION}/set-of-semantic-properties-extended.json\n\n" >> ${LOCAL_SETTINGS}
printf "# set of classes (domain/range) extracted from GitTables semantic properties\n" >> ${LOCAL_SETTINGS}
printf "gitTables_classes=${LOCAL_CLASSIFICATION}/GitTables-classes.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# set of semantic properties provided by GitTables\n" >> ${LOCAL_SETTINGS}
printf "gitTables_semantic_properties=${LOCAL_CLASSIFICATION}/GitTables-semantic-properties.json\n\n" >> ${LOCAL_SETTINGS}
printf "# class hierarchy for Schema.org classes\n" >> ${LOCAL_SETTINGS}
printf "schemaorg_class_hierarchy=${LOCAL_CLASSIFICATION}/class-hierarchy-schemaorg-cleaned.json\n\n" >> ${LOCAL_SETTINGS}
printf "# synonyms for name,designation,denomination file\n" >> ${LOCAL_SETTINGS}
printf "synonyms_denomination_file=${LOCAL_CLASSIFICATION}/words-related-to-denomination.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person)\n" >> ${LOCAL_SETTINGS}
printf "mapping_classes_to_categories=${LOCAL_CLASSIFICATION}/mapping-classes-to-categories.json\n\n" >> ${LOCAL_SETTINGS}

printf "# rdf quotient (used to summarize rdf inputs)\n" >> ${LOCAL_SETTINGS}
printf "rdf_quotient=${ABSTRA_DIR}/RDFQuotient-2.2-with-dependencies.jar\n\n" >> ${LOCAL_SETTINGS}


# add to config-heideltime.props the absolute path to TreeTagger
printf "\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props
printf "#### GENERATED PARAMETERS\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props
printf "treeTaggerHome=${LOCAL_TT}\n\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props

cp ${LOCAL_SETTINGS} core/src/main/resources
cp ${LOCAL_SETTINGS} gui/WebContent/WEB-INF