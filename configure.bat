rem @ECHO off

rem check number of parameters
IF NOT "%~2"=="" GOTO START
ECHO This script requires the following parameter:
ECHO - absolute path to Abstra working directory
rem ECHO - absolute path to Tomcat folder or # if no Tomcat is not installed
ECHO Example:
rem ECHO %~nx0 C:\abstraction-work\abstra-working-dir #
ECHO %~nx0 C:\abstraction-work\abstra-working-dir
GOTO :EOF

:START

set ABSTRA_DIR=%~1
set ESC_ABSTRA_DIR=%ABSTRA_DIR:\=\\%
rem set TOMCAT_DIR=%~2
rem set ESC_TOMCAT_DIR=%TOMCAT_DIR:\=\\%

ECHO ABSTRA_DIR is: '%ABSTRA_DIR%'
ECHO ESC_ABSTRA_DIR is: '%ESC_ABSTRA_DIR%'
rem ECHO TOMCAT_DIR is: '%TOMCAT_DIR%'
rem ECHO ESC_TOMCAT_DIR is: '%ESC_TOMCAT_DIR%'


set ROOT=core\lib
set CACHE_DIR=cache
set TMP_DIR=tmp
set TREETAGGER_DIR=treetagger
set MODELS_DIR=models
set CMD_DIR=cmd
set LIB_DIR=lib
set SCRIPTS_DIR=scripts
set HEIDELTIME_DIR=heideltime
set CLASSIFICATION_DIR=classification
set REPORTING_DIR=reporting

set ABSTRA_TT=%ROOT%\%TREETAGGER_DIR%
set ABSTRA_TT_BIN=%ABSTRA_TT%\binWindows
set ABSTRA_TT_CMD=%ABSTRA_TT%\%CMD_DIR%
set ABSTRA_TT_LIB=%ABSTRA_TT%\%LIB_DIR%
set ABSTRA_TT_MODELS=%ABSTRA_TT%\%MODELS_DIR%
set ABSTRA_HEIDELTIME=%ROOT%\%HEIDELTIME_DIR%
set ABSTRA_SCRIPTS=%SCRIPTS_DIR%
set ABSTRA_CLASSIFICATION=%ROOT%\%CLASSIFICATION_DIR%
set ABSTRA_REPORTING=%ROOT%\%REPORTING_DIR%

set LOCAL_CACHE=%ABSTRA_DIR%\%CACHE_DIR%
set LOCAL_TMP=%ABSTRA_DIR%\%TMP_DIR%
set LOCAL_TT=%ABSTRA_DIR%\%TREETAGGER_DIR%
set LOCAL_TT_BIN=%LOCAL_TT%\bin
set LOCAL_TT_CMD=%LOCAL_TT%\%CMD_DIR%
set LOCAL_TT_LIB=%LOCAL_TT%\%LIB_DIR%
set LOCAL_TT_MODELS=%LOCAL_TT%\%MODELS_DIR%
set LOCAL_HEIDELTIME=%ABSTRA_DIR%\%HEIDELTIME_DIR%
set LOCAL_SCRIPTS=%ABSTRA_DIR%\%SCRIPTS_DIR%
set LOCAL_CLASSIFICATION=%ABSTRA_DIR%\%CLASSIFICATION_DIR%
set LOCAL_REPORTING=%ABSTRA_DIR%\%REPORTING_DIR%
set LOCAL_PYTHON_ENV=%ABSTRA_DIR%\abstra_env
set LOCAL_SETTINGS=%ABSTRA_DIR%\local.settings



IF NOT EXIST %ABSTRA_DIR% ( mkdir %ABSTRA_DIR% )
IF NOT EXIST %LOCAL_CACHE% ( mkdir %LOCAL_CACHE% )
IF NOT EXIST %LOCAL_TMP% ( mkdir %LOCAL_TMP% )
IF NOT EXIST %LOCAL_TT% ( mkdir %LOCAL_TT% )
IF NOT EXIST %LOCAL_TT_BIN% ( mkdir %LOCAL_TT_BIN% )
IF NOT EXIST %LOCAL_TT_CMD% ( mkdir %LOCAL_TT_CMD% )
IF NOT EXIST %LOCAL_TT_LIB% ( mkdir %LOCAL_TT_LIB% )
IF NOT EXIST %LOCAL_TT_MODELS% ( mkdir %LOCAL_TT_MODELS% )
IF NOT EXIST %LOCAL_HEIDELTIME% ( mkdir  %LOCAL_HEIDELTIME% )
IF NOT EXIST %LOCAL_SCRIPTS% ( mkdir  %LOCAL_SCRIPTS% )
IF NOT EXIST %LOCAL_CLASSIFICATION% ( mkdir  %LOCAL_CLASSIFICATION% )
IF NOT EXIST %LOCAL_REPORTING% ( mkdir  %LOCAL_REPORTING% )


xcopy %ABSTRA_TT_BIN% %LOCAL_TT_BIN%
xcopy %ABSTRA_TT_CMD% %LOCAL_TT_CMD%
xcopy %ABSTRA_TT_LIB% %LOCAL_TT_LIB%
xcopy %ABSTRA_TT_MODELS% %LOCAL_TT_MODELS%
xcopy /s %ABSTRA_SCRIPTS% %LOCAL_SCRIPTS% 
xcopy %ABSTRA_HEIDELTIME% %LOCAL_HEIDELTIME%
xcopy %ABSTRA_REPORTING% %LOCAL_REPORTING%
xcopy %ABSTRA_CLASSIFICATION% %LOCAL_CLASSIFICATION%
xcopy /F /Q core\lib\RDFQuotient-2.2-with-dependencies.jar %ABSTRA_DIR%\.
xcopy /F /Q core\src\main\resources\parameter.settings %ABSTRA_DIR%\local.settings


py -m venv %LOCAL_PYTHON_ENV%
%LOCAL_PYTHON_ENV%\Scripts\python -m pip install --upgrade pip
%LOCAL_PYTHON_ENV%\Scripts\pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html



rem build escaped paths for each resource to be written in the local.settings (Java removes single backslashes)
set ESC_LOCAL_CACHE=%ESC_ABSTRA_DIR%\\%CACHE_DIR%
set ESC_LOCAL_TMP=%ESC_ABSTRA_DIR%\\%TMP_DIR%
set ESC_LOCAL_TT=%ESC_ABSTRA_DIR%\\%TREETAGGER_DIR%
set ESC_LOCAL_TT_BIN=%ESC_LOCAL_TT%\\bin
set ESC_LOCAL_TT_CMD=%ESC_LOCAL_TT%\\%CMD_DIR%
set ESC_LOCAL_TT_LIB=%ESC_LOCAL_TT%\\%LIB_DIR%
set ESC_LOCAL_TT_MODELS=%ESC_LOCAL_TT%\\%MODELS_DIR%
set ESC_LOCAL_HEIDELTIME=%ESC_ABSTRA_DIR%\\%HEIDELTIME_DIR%
set ESC_LOCAL_SCRIPTS=%ESC_ABSTRA_DIR%\\%SCRIPTS_DIR%
set ESC_LOCAL_CLASSIFICATION=%ESC_ABSTRA_DIR%\\%CLASSIFICATION_DIR%
set ESC_LOCAL_REPORTING=%ESC_ABSTRA_DIR%\\%REPORTING_DIR%
set ESC_LOCAL_PYTHON_ENV=%ESC_ABSTRA_DIR%\\abstra_env
set LOCAL_SETTINGS=%ABSTRA_DIR%\local.settings


rem add to local.settings the  absolute path to models
(ECHO.
ECHO.
ECHO #### GENERATED PARAMETERS
ECHO.) >> %LOCAL_SETTINGS%

ECHO # Temporary directory for Abstra >> %LOCAL_SETTINGS%
ECHO temp_dir=%ESC_LOCAL_TMP% >> %LOCAL_SETTINGS%

rem ECHO # Tomcat directory for Abstra GUI >> %LOCAL_SETTINGS%
rem ECHO tomcat_dir=%ESC_TOMCAT_DIR% >> %LOCAL_SETTINGS%

ECHO # Abstra cache location >> %LOCAL_SETTINGS%
ECHO cache_location=%ESC_LOCAL_CACHE% >> %LOCAL_SETTINGS%

ECHO # Python location >> %LOCAL_SETTINGS%
ECHO python_path=%ESC_LOCAL_PYTHON_ENV%\\bin\\python >> %LOCAL_SETTINGS%

ECHO # Path to python scripts (Flair extraction) >> %LOCAL_SETTINGS%
ECHO python_script_location=%ESC_LOCAL_SCRIPTS% >> %LOCAL_SETTINGS%
ECHO data_processing_script_location=%ESC_LOCAL_SCRIPTS%\\data_preprocessing >> %LOCAL_SETTINGS%

ECHO # Treetagger location >> %LOCAL_SETTINGS%
ECHO treetagger_home=%ESC_LOCAL_TT% >> %LOCAL_SETTINGS%

ECHO # Stanford models location >> %LOCAL_SETTINGS%
ECHO stanford_models=%ESC_LOCAL_TT_MODELS% >> %LOCAL_SETTINGS%

ECHO # Configuration file used by HeidelTime (date extractor) >> %LOCAL_SETTINGS%
ECHO config_heideltime=%ESC_LOCAL_HEIDELTIME%\\config-heideltime.props >> %LOCAL_SETTINGS%

ECHO # Word2Vec model location (used for classification during abstraction) >> %LOCAL_SETTINGS%
ECHO word_embedding_model_path=%ESC_LOCAL_CLASSIFICATION%\\word2vec.bin >> %LOCAL_SETTINGS%
ECHO stop_words_english=%ESC_LOCAL_CLASSIFICATION%\\stop_words_english.txt >> %LOCAL_SETTINGS%
ECHO stop_words_french=%ESC_LOCAL_CLASSIFICATION%\\stop_words_french.txt >> %LOCAL_SETTINGS%

rem set of semantic resources for the classification
ECHO # set of manually-defined semantic properties >> %LOCAL_SETTINGS%
ECHO set_of_semantic_properties_manual=%ESC_LOCAL_CLASSIFICATION%\\set-of-semantic-properties-manual.json >> %LOCAL_SETTINGS%
ECHO # set of semantic properties extended with Yago and DBPedia triples >> %LOCAL_SETTINGS%
ECHO set_of_semantic_properties_extended=%ESC_LOCAL_CLASSIFICATION%\\set-of-semantic-properties-extended.json >> %LOCAL_SETTINGS%
ECHO # set of classes (domain/range) extracted from GitTables semantic properties >> %LOCAL_SETTINGS%
ECHO gitTables_classes=%ESC_LOCAL_CLASSIFICATION%\\GitTables-classes.txt >> %LOCAL_SETTINGS%
ECHO # get of semantic properties provided by GitTables >> %LOCAL_SETTINGS%
ECHO gitTables_semantic_properties=%ESC_LOCAL_CLASSIFICATION%\\GitTables-semantic-properties.json >> %LOCAL_SETTINGS%
ECHO # class hierarchy of Schema.org classes >> %LOCAL_SETTINGS%
ECHO schemaorg_class_hierarchy=%ESC_LOCAL_CLASSIFICATION%\\class-hierarchy-schemaorg-cleaned.json >> %LOCAL_SETTINGS%
ECHO # synonyms for name,designation,denomination file >> %LOCAL_SETTINGS%
ECHO synonyms_denomination_file=%ESC_LOCAL_CLASSIFICATION%\\words-related-to-denomination.txt >> %LOCAL_SETTINGS%
ECHO # mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person) >> %LOCAL_SETTINGS%
ECHO mapping_classes_to_categories=%ESC_LOCAL_CLASSIFICATION%\\mapping-classes-to-categories.json >> %LOCAL_SETTINGS%

ECHO # rdf quotient (used to summarize rdf inputs) >> %LOCAL_SETTINGS%
ECHO rdf_quotient=%ESC_ABSTRA_DIR%\\RDFQuotient-2.2-with-dependencies.jar >> %LOCAL_SETTINGS%

ECHO # graphviz location (overwrites the previous default location) >> %LOCAL_SETTINGS%
ECHO drawing.dot_installation=C:\\Program Files\\Graphviz\\bin\\dot.exe >> %LOCAL_SETTINGS%

rem add to config-heideltime.props the absolute path to TreeTagger
(ECHO.
 ECHO.
 ECHO #### GENERATED PARAMETERS 
 ECHO.) >> %LOCAL_HEIDELTIME%\config-heideltime.props
ECHO treeTaggerHome=%LOCAL_TT% >> %LOCAL_HEIDELTIME%\config-heideltime.props


copy %LOCAL_SETTINGS% core\src\main\resources
copy %LOCAL_SETTINGS% gui\WebContent\WEB-INF
